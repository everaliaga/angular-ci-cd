import { TestBed } from '@angular/core/testing';
import { ConfigProxyServiceMock } from './config-proxy.service.mock';


describe('ConfigProxyServiceMock', () => {
    beforeEach(() => TestBed.configureTestingModule({}));

    it('should be created', () => {
        const service: ConfigProxyServiceMock = TestBed.get(ConfigProxyServiceMock);
        expect(service).toBeTruthy();
    });
});
