import { HttpClientModule } from '@angular/common/http';
import { async, TestBed } from '@angular/core/testing';
import { ConfigProxyService } from './config-proxy.service';


describe('ConfigProxyService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule],
        providers: [ConfigProxyService]
    }));

    it('should be created', () => {
        const service: ConfigProxyService = TestBed.get(ConfigProxyService);
        expect(service).toBeTruthy();
    });

    it('should get configutarion', async(() => {
        const service: ConfigProxyService = TestBed.get(ConfigProxyService);
        service.getConfig().subscribe(
            config => expect(config).not.toBeNull()
        );
    }));

});
