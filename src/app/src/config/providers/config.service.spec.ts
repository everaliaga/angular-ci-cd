import { TestBed } from '@angular/core/testing';
import { ConfigProxyService } from './config-proxy.service';
import { ConfigProxyServiceMock } from './config-proxy.service.mock';
import { ConfigService } from './config.service';


describe('ConfigService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            ConfigService,
            { provide: ConfigProxyService, useClass: ConfigProxyServiceMock }
        ]
    }));

    it('should be created', () => {
        const service: ConfigService = TestBed.get(ConfigService);
        expect(service).toBeTruthy();
    });

    it('should load configuration', () => {
        const service: ConfigService = TestBed.get(ConfigService);
        service.load();
        expect(service.config.api).not.toBeNull();
    });

});
