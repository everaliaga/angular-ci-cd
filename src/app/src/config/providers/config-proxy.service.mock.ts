import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Config } from '../models/config';

@Injectable({
    providedIn: 'root'
})
export class ConfigProxyServiceMock {

    CONFIG_MOCK: Config  = {
        api : 'http://api-back:8070/'
    };

    constructor() { }

    getConfig(): Observable<Config> {
        return of(this.CONFIG_MOCK);
    }
}
