import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Config } from '../models/config';

@Injectable({
    providedIn: 'root'
})
export class ConfigProxyService {

    constructor(private http: HttpClient) { }

    getConfig(): Observable<Config> {
        return this.http.get<Config>(`${environment.configFile}`);
    }

}
