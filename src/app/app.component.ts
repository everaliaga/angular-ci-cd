import { Component, OnInit } from '@angular/core';
import { Config } from './src/config/models/config';
import { ConfigService } from './src/config/providers/config.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    config: Config;

    constructor(private configService: ConfigService) { }

    ngOnInit() {
        this.config = this.configService.config;
        console.log(this.configService.config);
    }
}
